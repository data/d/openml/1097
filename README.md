# OpenML dataset: ICU

https://www.openml.org/d/1097

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

Datasets of Data And Story Library, project illustrating use of basic statistic methods, converted to arff format by Hakan Kjellerstrand.
Source: TunedIT: http://tunedit.org/repo/DASL

DASL file http://lib.stat.cmu.edu/DASL/Datafiles/ICU.html

Reference:
Authorization:   Contact authors
Description:   The data consist of 200 subjects from a larger study on the survival of patients following admission to an adult intensive care unit (ICU).  The study used logistic regression to predict the probability of survival for these patients until their discharge from the hospital.  The dependent variable is the binary variable Vital Status (STA).  Nineteen possible predictor variables, both discrete and continuous, were also observed.

Number of cases:   200
Variable Names:

ID:   ID number of the patient
STA:   Vital status (0 = Lived, 1 = Died)
AGE:   Patient's age in years
SEX:   Patient's sex (0 = Male, 1 = Female)
RACE:   Patient's race (1 = White, 2 = Black, 3 = Other)
SER:   Service at ICU admission (0 = Medical, 1 = Surgical)
CAN:   Is cancer part of the present problem? (0 = No, 1 = Yes)
CRN:   History of chronic renal failure (0 = No, 1 = Yes)
INF:

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1097) of an [OpenML dataset](https://www.openml.org/d/1097). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1097/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1097/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1097/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

